#include <iostream>
#include <math.h>
#include <TNL/Matrices/SparseMatrix.h>
#include <TNL/Devices/Host.h>
#include <TNL/Containers/Vector.h>
#include <TNL/Solvers/Linear/Jacobi.h>
using namespace std;
//We are implementing a poisson equation solver in 1D which could solve equations of the form : -u''(x)=f(x).
//The boundary conditions, value of discretization and the function f(x) are set up in the main function.

//Let's start by defining a Poisson1D class
template <typename Real=double, typename Index=int>
class Poisson1D {
public:
    Poisson1D(Index n, Real start, Real end);
    void solve();Real f_value_;

private:
    Real f(Real x);
    Real integration(Real i, Real h);
    Real start_,end_,h_;
    Index num_points_,n_;
    std::vector<Real> points_;
};

//Constructor
template <typename Real, typename Index>
Poisson1D<Real, Index>::Poisson1D(Index n, Real start, Real end)
    : n_(n), start_(start), end_(end)
{
    h_ = (end_ - start_) / n_;
    num_points_ = n_ + 2;
}

// Define the function to integration
template <typename Real, typename Index>
Real Poisson1D<Real, Index>::f(Real x)
{
    return f_value_;   
}

// integration using Simpson's rule
template <typename Real, typename Index>
Real Poisson1D<Real, Index>::integration(Real i, Real h){
    const Real dx = 1e-6;
    const Real  beg = i - h_;
    const Real  finish = i + h_;
    const int n = static_cast<int>((finish - beg) / dx) + 1;
    const Real  delta_x = (finish - beg) / static_cast<double>(n - 1);

    Real  sum = 0;
    Real x = beg;
    sum += f(x);

    for (int j = 1; j < n - 1; ++j) {
        x += delta_x;
        if (j % 2 == 0) {
            sum += 2 * f(x);
        } else {
            sum += 4 * f(x);
        }
    }

    sum += f(finish);
    sum *= delta_x / 3.0;
    return sum;
}

//Equation solver
template <typename Real, typename Index>
void Poisson1D<Real, Index>::solve()
{
   
   // Define the stiffness matrix
    TNL::Matrices::SparseMatrix<Real, TNL::Devices::Host> stiffness_matrix_(n_ + 2, n_ + 2);
    std::map<std::pair<Index, Index>, Real> elements;
    for (Index i = 1; i <= n_; ++i) {
        elements[std::make_pair(i, i)] = 2 / h_;
        if(i>1)
        elements[std::make_pair(i, i - 1)] = -1 / h_;
        if(i<n_)
        elements[std::make_pair(i, i + 1)] = -1 / h_;
    }
    //Set the boundary conditions
    elements[std::make_pair(0, 0)] = 1;
    elements[std::make_pair(n_ + 1, n_ + 1)] = 1;

    stiffness_matrix_.setElements(elements);

    // cout<<"Stiffness Matrix:\n"<<stiffness_matrix_<<'\n';
    TNL::Containers::Vector<Real,TNL::Devices::Host> load_vector(n_+2,0.0);  
    std::vector <Real> load_element;

    //Set the load vector
    load_element.push_back(0);
    for(Index i=1;i<=n_;i++)
    {
        Real val=integration(Real(i)/((Real(n_))+1),h_);
        load_element.push_back(val);
    }
    load_element.push_back(0);  
    load_vector.forAllElements([&](Index i, Real& val) { val = load_element[i]; } ); 

    //Solve the system
    TNL::Containers::Vector<Real,TNL::Devices::Host> x(n_+2,1.0);
    auto matrix_ptr = std::make_shared< TNL::Matrices::SparseMatrix<Real, TNL::Devices::Host> >(stiffness_matrix_);
    TNL::Solvers::Linear::Jacobi< TNL::Matrices::SparseMatrix<Real, TNL::Devices::Host> > solver;
    solver.setMatrix( matrix_ptr ); 
    solver.solve(load_vector, x );
    std::cout<<"Solution:"<<x<<'\n';
   
}
//Main function
int main()
{
    int n;
    std::cout<<"Enter n for discretization of domain."<<endl;
    std::cin>>n;
    //Assuming dirichlet boundary conditions
    Poisson1D<double, int> poisson1d(n, 0.0, 1.0);
    cout<<"Enter the value of f(x) for the equation -u''(x)=f(x) in the domain [0,1]."<<endl;
    cin>>poisson1d.f_value_;
    poisson1d.solve();
    return 0;
}
