# 1D_Poisson_Solver_FEM 

The Poisson equation solver in 1D has been implemented using the Finite Element Method and the TNL Library.
The solver is capable of working out equations on the form: 

                  -u''(x)=f(x)

The problem is solved in the following steps :

### Step 1: Discretization of the domain based on the user input of number of nodes.
### Step 2: Define the stiffness matrix (K).
### Step 3: Set up the boundary conditions.
### Step 4: Set up the load vector using integration by Simpson's rule and basis functions.
### Step 5: Solve the linear system using Jacobi.(Ku=f)

Please refer to the Output file to see a few output examples.

